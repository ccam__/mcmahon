# Design and Code Thinking

I decide to group the templates into three groups base on their similarities. 1 and 3, 2 and 4, and 5.

## enl_template_1.html and enl_template_3.html

### Design:

### Heading

I believe that the logo should be above the ad. It shows that the newsletter is isn't absolutely controlled by the ads. It also eases the reader in to the letter. The bottom border bellow divides the logo from the ad, it could be taken out for a smother transition.

### Top Artilces

There should be a heading to the articles. It is a little more formatted and uniform as you'll see with the multimedia and web only sections. Most people are right handed so i moved button to the right for mobile to make it easier to tap.

### First and second ad

These should be a banner ads. It could be shorter like the Buyers Guide ad.

### Multimedia and Web Only

I chose to make these headings. We could possibly make the sections have more articles.

### Footer

I liked the footer you already have I only changed the backgrouund to add a little more color. I used GIMP to add a layer and invert the colors.

### Code:

In both of these templates I mixed the css, both inline and classes. I know most email clients strip out `<style>` tags so I tried to keep as much as inlined as possilbe.

## enl_template_2.html and enl_template_4.html

## Design:

### Heading

Again I belive the logo should be above the ad. No bottom border this time to show the difference in style.

### Body

With the articles added a background color and designed them to look like a card.

### Footer.

I keep the footer consistent through all of designs. Just changing the colors.

### Code:

With these two templates I treadted them more as a web page to demonstrate my knowledge of CSS better. I used a grid instead of a table to construct the layout. In moblie I moved the second article above the ad to fit the "two articles then an ad" rule.

## enl_template_5.html

### Design:

### Heading

It was a challenge to incorporate the cover image, I feel like this section is a little crowded.

### Body

I wanted to add more color into this design so I took the card idea from enl_template_2.html and added it here while changing the back ground color to a blue complementary to the red button. It is a sinlge column design to make it easier to optomize for mobile.

### Footer

Again pretty much the same as the others just a color change.

### Code:

With this template I went back to a pure table and inline styling. I have only user agent reset css in the style tag.
